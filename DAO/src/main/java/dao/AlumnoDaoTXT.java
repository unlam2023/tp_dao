/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import calendario.MiCalendario;
import calendario.MiCalendarioException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import persona.Alumno;
import persona.Persona;
import persona.PersonaInvalidaException;
import persona.PersonaNombreException;

/**
 *
 * @author g.guzman
 */
public class AlumnoDaoTXT extends DAO<Alumno,Integer>{

    private static final String RWS_MODE = "rws";
    
    private RandomAccessFile raf;
    
    AlumnoDaoTXT(String pathFilename) throws DaoException {
        try {
            raf = new RandomAccessFile(new File(pathFilename), RWS_MODE);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AlumnoDaoTXT.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException("Error de I/O =>"+ex.getLocalizedMessage());
        }
    }

    
    @Override
    public void create(Alumno alumno) throws DaoException {
        
        try {
            if (exist(alumno.getDni())) {
                throw new DaoException("El alumno ya existe (DNI="+alumno.getDni()+")");
            }
            raf.seek(raf.length());
            raf.writeBytes("\n");
            raf.writeBytes(alumno.toString()+Persona.DELIM);
        } catch (IOException ex) {
            Logger.getLogger(AlumnoDaoTXT.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException("Error al crear el alumno =>"+ex.getLocalizedMessage());
        }
    }

    @Override
    public boolean exist(Integer buscarDni) throws DaoException {
        try {
            raf.seek(0);
            String linea;
            String[] campos;
            Integer dni;
            while ((linea = raf.readLine())!=null) {
                campos = linea.split(Persona.DELIM);
                dni = Integer.valueOf(campos[0]);
                if (buscarDni.equals(dni)) {
                    return true;
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(AlumnoDaoTXT.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException("Error E/S =>"+ex.getLocalizedMessage());
        }
        return false;
    }

    @Override
    public Alumno read(Integer dni) {
        Alumno alu = new Alumno();



        
        return alu;
    }

    @Override
    public void update(Alumno alumno) throws DaoException {
        try {
            raf.seek(0);
            String linea;
            String[] campos;
            int dni;
            long pos = 0;
            while ((linea = raf.readLine())!=null) {
                campos = linea.split(Persona.DELIM);
                dni = Integer.valueOf(campos[0]);
                if (alumno.getDni()==dni) {
                    //raf.seek(raf.getFilePointer()-linea.length());
                    // habria borrar todo la linea primero para que no quede sucio 
                    raf.seek(pos);
                    raf.writeBytes(alumno.toString());
                    return;
                }
                pos = raf.getFilePointer();
            }
        } catch (IOException ex) {
            Logger.getLogger(AlumnoDaoTXT.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException("Error al actualizar =>"+ex.getLocalizedMessage());
        }
    }

    @Override
    public void deleteById(Integer dni) {
        System.out.println("DNI: "+dni);
        
    }

    @Override
    public void delete(Alumno entidad) throws DaoException {
        entidad.setActivo(false);
        update(entidad);
    }

    @Override
    public List<Alumno> findAll(boolean onlyActive) throws DaoException {
        List<Alumno> alu = new ArrayList<>();
        try {
            raf.seek(0);
            String linea;
            String[] campos;
            while ((linea = raf.readLine())!=null) {
                campos = linea.split(Persona.DELIM);
                if(onlyActive){
                    if(Boolean.valueOf(campos[5].trim())){
                        Integer dni = Integer.valueOf(campos[0]);
                        String[] fecha = campos[4].split("/");
                        alu.add(new Alumno(dni, campos[1], campos[2], campos[3].charAt(0), new MiCalendario(Integer.valueOf(fecha[0]), Integer.valueOf(fecha[1]),Integer.valueOf(fecha[2])),Boolean.valueOf(campos[5].trim())));
                    }
                }else{
                   Integer dni = Integer.valueOf(campos[0]);
                   String[] fecha = campos[4].split("/");
                   alu.add(new Alumno(dni, campos[1], campos[2], campos[3].charAt(0), new MiCalendario(Integer.valueOf(fecha[0]), Integer.valueOf(fecha[1]),Integer.valueOf(fecha[2])),Boolean.valueOf(campos[5].trim())));
                     
                }    
            }
        } catch (IOException ex) {
            Logger.getLogger(AlumnoDaoTXT.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException("Error E/S =>"+ex.getLocalizedMessage());
        } catch (MiCalendarioException ex) {
            Logger.getLogger(AlumnoDaoTXT.class.getName()).log(Level.SEVERE, null, ex);
        } catch (PersonaInvalidaException ex) {
            Logger.getLogger(AlumnoDaoTXT.class.getName()).log(Level.SEVERE, null, ex);
        } catch (PersonaNombreException ex) {
            Logger.getLogger(AlumnoDaoTXT.class.getName()).log(Level.SEVERE, null, ex);
        }
        return alu;
    }

    @Override
    public void close() throws DaoException {
        try {
            raf.close();
        } catch (IOException ex) {
            Logger.getLogger(AlumnoDaoTXT.class.getName()).log(Level.SEVERE, null, ex);
            throw new DaoException("Error al cerrar el archivo");
        }
    }

}
