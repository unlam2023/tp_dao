/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package persona;

/**
 *
 * @author g.guzman
 */
public class PersonaInvalidaException extends Exception {

    public PersonaInvalidaException(String mensaje) {
        super(mensaje);
    }
    
}
